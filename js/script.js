(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.ei = {
    attach: function (context, settings) {
      
      var ei = {
          init: function(){
              ei.scrollTop();
          },
          
          scrollTop: function(){
            
            $("a#scroll-to-top").once().each(function(){
                $(this).click(function() {
                  $("html, body").animate({ scrollTop: 0 }, "slow");
                  return false;
                });
            });
              
          },
      }
      
      ei.init();
      
    }
  };

})(jQuery, Drupal, drupalSettings);